#!/bin/bash

### ----------------------------------------------------------------------------------------------------------- ###
###                       Automatically Provision Elasticsearch Datasources In Grafana                          ###
###                                                    v1.0                                                     ###
###                                                                                                             ###
###    This Script Is For Adding All Of Your Elasticsearch Index's (Or Alias If They Have One Assigned) Into    ###
###                                          Your Grafana Installation.                                         ###
###                                                                                                             ###
###   It Requires Elasticsearch Credentials (Security Is Opensource From X-Pack So Should Be Enabled) As Well   ###
###    As A Grafana API Key To Create The Datasources If They Do Not Exist. If All Datasources Already Exist    ###
###                                 The Script Will Exit Without Taking Action.                                 ###
###                                                                                                             ###
###           Please Report Any Issues Found To: https://gitlab.com/apenketh-opensource/grafana-tools           ###
###                                                                                                             ###
###  Example Way To Run:                                                                                        ###
###    ./elastic-ds-prov.sh                                                                                     ###
### ----------------------------------------------------------------------------------------------------------- ###

### *** Please Fill In Your Host & Credential Info Before Running *** ###
GRAFANA_URL="http://localhost:3000"
GRAFANA_API_KEY=""
ELASTIC_URL="http://localhost:9200"
ELASTIC_USER="" # This User Will Be Used For Both Querying Elastic & Datasource Provisioning
ELASTIC_PASSWD=""
ES_VERSION="70" # Version 6.X=60, 7.X=70
TIMEFIELD="@timestamp" # Field Used For Timestamp In Elastic
### *** --------------------------------------------------------- *** ###

## Pre-Req's
### Make Sure Required Packages Are Installed
if command -v jq >/dev/null 2>&1; then
  :
else
  echo "Please Make Sure The 'jq' Package Is Installed On Your System - 'https://stedolan.github.io/jq/download/'"
  exit
fi

### Get Our Elastic Cluster Name to Be Used In The Datasource Name
ELASTIC_NAME=$(curl -s -XGET -u $ELASTIC_USER:$ELASTIC_PASSWD "$ELASTIC_URL/" | jq -r '.name')


## Get All Datasource Names To Add From Elasticsearch
### Get Index List & Check If Index's Have Assigned Alias - If Not Remove Date
for INDICES in $(curl -s -XGET -u $ELASTIC_USER:$ELASTIC_PASSWD "$ELASTIC_URL/_cat/indices?h=index&s=index" | sed '/^\./ d'); do
  ALIAS_NAME=$(curl -s -XGET -u $ELASTIC_USER:$ELASTIC_PASSWD "$ELASTIC_URL/$INDICES/_alias" | jq -r '.[].aliases | keys | .[]')
  if [ -z "$ALIAS_NAME" ]; then
    GRAFANA_INDEX="$GRAFANA_INDEX
$(echo $INDICES | sed -r 's/[0-9]{4}\.[0-9]{2}\.[0-9]{2}//')"
  else
    GRAFANA_INDEX="$GRAFANA_INDEX
$ALIAS_NAME"
  fi
done

### Sanitize Output Removing Duplicates & Empty Lines
GRAFANA_INDEX=$(echo "$GRAFANA_INDEX" | uniq | awk 'NF')


## Add New Datasources To Grafana
### Function To Check If Datasource Exist's & If Not Submit To Grafana
GRAFANA_SUBMIT () {
  if [ "$(curl -s -H "Authorization: Bearer $GRAFANA_API_KEY" "$GRAFANA_URL/api/datasources/id/$ELASTIC_NAME|$2" | jq -r '.[]')" == "Data source not found" ]; then
    curl -s -X POST -H "Authorization: Bearer $GRAFANA_API_KEY" -H "Accept: application/json" -H "Content-Type: application/json" \
    -d '{ "orgId": 1, "name": "'"$ELASTIC_NAME|$2"'","Type": "elasticsearch","access": "proxy","url": "'"$ELASTIC_URL"'","database": "'"$1"'","basicAuth": true,"basicAuthUser": "'"$ELASTIC_USER"'","secureJsonData": {"basicAuthPassword": "'"$ELASTIC_PASSWD"'"},"isDefault": false,"jsonData": {"esVersion": '"$ES_VERSION"',"interval": "Daily","keepCookies": [],"logLevelField": "","logMessageField": "","maxConcurrentShardRequests": 5,"timeField": "'"$TIMEFIELD"'" }}' \
    $GRAFANA_URL/api/datasources
  else
    :
  fi
}

### Check If The Datasource Is An Index Or Alias's And Call Submit Function
for G_INDEX in $GRAFANA_INDEX; do
  if [[ "$G_INDEX" =~ '-'$ ]]; then
    GRAFANA_SUBMIT "[$G_INDEX]YYYY.MM.DD" "${G_INDEX%?}"
  else
    GRAFANA_SUBMIT "[$G_INDEX]" "$G_INDEX"
  fi
done
