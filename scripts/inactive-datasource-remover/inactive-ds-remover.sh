#!/bin/bash

### ----------------------------------------------------------------------------------------------------------- ###
###                                Remove Grafana Datasource's Not In Active Use                                ###
###                                                    v1.0                                                     ###
###                                                                                                             ###
###     ** Note ** This Script Has Been Built To Be Used In Conjunction with The JSON Backup Ansible Roll       ###
###                                          Which Can Be Found Here:                                           ###
###      https://gitlab.com/apenketh-opensource/grafana-tools/tree/master/ansible-roles/json-file-backup        ###
###                                                                                                             ###
###     This Script Can Be Used To Find & Delete Grafana Datasources Which Are Not Used In Any Dashboards       ###
###   Combined With Inactive Dashboard Remover Script Will Allow You To Effectivly Clean Up Two Major Area's    ###
###                                                 Of Grafana.                                                 ###
###                                                                                                             ###
###           Please Report Any Issues Found To: https://gitlab.com/apenketh-opensource/grafana-tools           ###
###                                                                                                             ###
###  Example Way To Run:                                                                                        ###
###    ./inactive-ds-remover.sh                                                                                 ###
### ----------------------------------------------------------------------------------------------------------- ###

### *** Please Fill In Your Host & Credential Info Before Running *** ###
GRAFANA_URL="http://localhost:3000"
GRAFANA_API_KEY=""
JSON_BACKUP_DIR=""
### *** --------------------------------------------------------- *** ###

## Pre-Req's
### Check Backup JSON Directory Exists
if [ -d "$JSON_BACKUP_DIR" ]; then
  :
else
  echo "Please Make Sure You Backup Your Grafana In JSON & Specify The Backup Dir Before Running This Script"
  exit
fi

### Make Sure Required Packages Are Installed
if command -v jq >/dev/null 2>&1; then
  :
else
  echo "Please Make Sure The 'jq' Package Is Installed On Your System - 'https://stedolan.github.io/jq/download/'"
  exit
fi


## Perform Search & Request Deletion If Needed
for GRAF_DS in $(curl -s -H "Authorization: Bearer $GRAFANA_API_KEY" "$GRAFANA_URL/api/datasources" | jq -r '.[].name'); do
  if grep -qR "$GRAF_DS" $JSON_BACKUP_DIR/dashboards/; then
    :
  else
    read -p "The Datasource '$GRAF_DS' Has Not Been Found. Do You Want To Permanently Remove This Datasource From Grafana? Please Select [y/N] " yn
    case $yn in
      [Yy]* )
        echo "Removing Datasource '$GRAF_DS'";
        curl -s -w "\n \n" -X DELETE -H "Authorization: Bearer $GRAFANA_API_KEY" "$GRAFANA_URL/api/datasources/name/$GRAF_DS";;
      [Nn]* )
        echo -e "Skipping '$GRAF_DS' \n";;
      * )
        echo "Please Select [y/N]";;
     esac
  fi
done

echo "Finished Processing All Datasources"
